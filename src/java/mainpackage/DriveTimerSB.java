/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainpackage;

import drivesimple.HuntListsToDB;
import drivesimple.HuntMessage;
import drivesimple.ManageData;
import drivesimple.MyFile;
import entitySimple.UserMailBox;
import entitySimple.WorkersDriveFiles;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import webEntity.SugMessageContactsFacade;
import webEntity.SugMessageFacade;
import webEntity.TadirutFacade;
import webEntity.UserMailBoxFacade;
import webEntity.WorkersDriveFilesFacade;

/**
 *
 * @author dev2
 */
@Stateless
public class DriveTimerSB {
    
    @EJB
    UserMailBoxFacade mailBoxFacade;
    
    @EJB
    WorkersDriveFilesFacade workersDriveFilesFacade;
    
    @EJB 
    SugMessageContactsFacade sgContactsController;
    
    @EJB 
    TadirutFacade tadirutController;
    
    @EJB
    SugMessageFacade smController;

    @Schedule(dayOfWeek = "Sun-Thu", month = "*", hour = "7-15", dayOfMonth = "*", year = "*", minute = "24", second = "0", persistent = false)
    public void myTimer() {
        

        List<UserMailBox> allWorkers = mailBoxFacade.findAll();
        List<String> histoGoogleID = workersDriveFilesFacade.getAllGoogleID();
        

        ManageData manageData = new ManageData();
        List<MyFile> newFilesList = new ArrayList<MyFile>();
        List<WorkersDriveFiles> listToInsert = new ArrayList<WorkersDriveFiles>();
        
        manageData.updateAll(histoGoogleID, allWorkers, newFilesList, listToInsert);
        
        List<HuntMessage> allMessagesList = new ArrayList<HuntMessage>();

        HuntListsToDB rowsToInsert = new HuntListsToDB();
        if (newFilesList.size() > 0) {
            manageData.sendMailOfNewFiles(newFilesList, allMessagesList, rowsToInsert);
        }

        workersDriveFilesFacade.insertList(listToInsert);
        tadirutController.insertList(rowsToInsert.getTadirutList());
        allMessagesList.stream().forEach(q -> q.initSugMessageWithTadirut());
        smController.insertList(rowsToInsert.getSmList());
        allMessagesList.stream().forEach(q->q.initSugMessageContacts());
        sgContactsController.insertList(rowsToInsert.getSmcList());

        System.out.println("finish drive " );
                
        System.out.println("Timer event: " + new Date());
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
