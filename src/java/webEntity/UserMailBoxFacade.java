/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webEntity;

import entitySimple.UserMailBox;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author dev2
 */
@Stateless
public class UserMailBoxFacade extends AbstractFacade<UserMailBox> {

    @PersistenceContext(unitName = "DriveUpgradePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserMailBoxFacade() {
        super(UserMailBox.class);
    }
    
}
