/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webEntity;

import entitySimple.Tadirut;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author dev2
 */
@Stateless
public class TadirutFacade extends AbstractFacade<Tadirut> {

    @PersistenceContext(unitName = "DriveUpgradePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TadirutFacade() {
        super(Tadirut.class);
    }

    public void insertList(List<Tadirut> list) {
        if(list == null)
            return;
        System.out.println("start to insert Tadirut . num of rows to insert: " + list.size());
        for (int i = 0; i < list.size(); i++) {
            em.persist(list.get(i));
        }
        em.flush();
        System.out.println("finish to insert Tadirut");
    }

}
