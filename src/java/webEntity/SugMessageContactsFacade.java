/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webEntity;

import entitySimple.SugMessageContacts;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author dev2
 */
@Stateless
public class SugMessageContactsFacade extends AbstractFacade<SugMessageContacts> {

    @PersistenceContext(unitName = "DriveUpgradePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SugMessageContactsFacade() {
        super(SugMessageContacts.class);
    }

    public void insertList(List<SugMessageContacts> list) {
        if (list == null) {
            return;
        }
        System.out.println("start to insert SugMessageContacts . num of rows to insert: " + list.size());
        for (int i = 0; i < list.size(); i++) {
            em.persist(list.get(i));
        }
        em.flush();
        System.out.println("finish to insert SugMessageContacts");
    }

}
