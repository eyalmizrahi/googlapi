/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webEntity;

import entitySimple.WorkersDriveFiles;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author dev2
 */
@Stateless
public class WorkersDriveFilesFacade extends AbstractFacade<WorkersDriveFiles> {

    @PersistenceContext(unitName = "DriveUpgradePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WorkersDriveFilesFacade() {
        super(WorkersDriveFiles.class);
    }

    public List<String> getAllGoogleID() {
//        EntityManager em = getEntityManager();
        Query q = em.createNamedQuery("WorkersDriveFiles.getAllGoogleID");
        if (q.getResultList() == null) {
            return new ArrayList<String>();
        }
        return q.getResultList();
    }

    public void insertList(List<WorkersDriveFiles> list) {
//        em.flush();
        int a = 1500;
        System.out.println("start to insert WorkersDriveFiles . num of rows to insert: " + list.size());
//        for (int j = 0, i = 0; j < list.size() / 1500; j++) {
        for (int i = 0; i < list.size(); i++) {

            em.persist(list.get(i));
//            for (int j = 0; j < a; i++) {
//                em.persist(list.get(j));
//            }
//            a += 1500;
            if (i % 1500 == 0) {
                em.flush();
            }
        }
//        }

        System.out.println("finish to insert WorkersDriveFiles  ");
    }

}
